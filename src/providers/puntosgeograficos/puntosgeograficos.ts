import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Global } from '../../app/app.global';

/*
  Generated class for the PuntosgeograficosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PuntosgeograficosProvider {

  constructor(public http: HttpClient) {
    console.log('Hello PuntosgeograficosProvider Provider');
  }
  public GETPunto() {

    return this.http.get(Global.url + "PuntoGeografico", { observe: 'response' });

  }
}
