import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../../app/app.global';

/*
  Generated class for the UsuarioProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UsuarioProvider {

  constructor(public http: HttpClient) {
    console.log('Hello UsuarioProvider Provider');
  }
  POSTLoginSAP(Username, Password) {

    let headers = new HttpHeaders();
   
    headers = headers.append("Authorization", "Basic " + btoa(Username+":"+Password));
   
    headers = headers.append("Content-Type", "application/x-www-form-urlencoded");
   
    return this.http.post(User.url + 'users/password', null, { observe: 'response', headers: headers });
   
    }
}
