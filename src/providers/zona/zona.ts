import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Global } from '../../app/app.global';
/*
  Generated class for the ZonaProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ZonaProvider {

  constructor(public http: HttpClient) {
    console.log('Hello ZonaProvider Provider');
  }
  public GETZona() {

    return this.http.get(Global.url + "Zona", { observe: 'response' });

  }
}
