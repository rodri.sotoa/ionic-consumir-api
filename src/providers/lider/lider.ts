import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Global } from '../../app/app.global';

/*
  Generated class for the LiderProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LiderProvider {

  constructor(public http: HttpClient) {
    console.log('Hello LiderProvider Provider');
  }
  public GETLider() {

    return this.http.get(Global.url + "Lider", { observe: 'response' });

  }
}
