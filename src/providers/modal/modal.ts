import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Global } from '../../app/app.global';

/*
  Generated class for the ModalProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ModalProvider {

  constructor(public http: HttpClient) {
    console.log('Hello ModalProvider Provider');
  }
  public GETIncidente(id) {

    return this.http.get(Global.url + "Reclamo/"+id, { observe: 'response' });

  }
}
