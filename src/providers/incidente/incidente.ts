import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Global } from '../../app/app.global';

/*
  Generated class for the IncidenteProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class IncidenteProvider {

  constructor(public http: HttpClient) {
    console.log('Hello IncidenteProvider Provider');
  }
  public GETIncidente() {

    return this.http.get(Global.url + "Reclamo", { observe: 'response' });

  }
  public SETIncidente(json){
    return this.http.post(Global.url+"Reclamo",json,{ observe: 'response', headers: { 'Content-Type': 'application/json' }});
  }
}
