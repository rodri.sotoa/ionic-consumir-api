import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Global } from '../../app/app.global';

/*
  Generated class for the DetalleriesgoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DetalleriesgoProvider {

  constructor(public http: HttpClient) {
    console.log('Hello DetalleriesgoProvider Provider');
  }

  public GETRiesgo() {

    return this.http.get(Global.url + "DetalleRiesgo", { observe: 'response' });

  }

  

}
