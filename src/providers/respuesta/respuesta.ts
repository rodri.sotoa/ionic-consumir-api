import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Global } from '../../app/app.global';

/*
  Generated class for the RespuestaProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RespuestaProvider {

  constructor(public http: HttpClient) {
    console.log('Hello RespuestaProvider Provider');
  }
  public GETRespuesta() {

    return this.http.get(Global.url + "Respuesta", { observe: 'response' });

  }
}
