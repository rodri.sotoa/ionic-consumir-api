import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoadingController, ToastController } from 'ionic-angular';
/*
  Generated class for the GlobalProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GlobalProvider {

  public loader: any;
  constructor(public http: HttpClient, public loadingCtrl: LoadingController, private toastCtrl: ToastController) {
    console.log('Hello GlobalProvider Provider');
  }

  public Loading() {
    this.loader = this.loadingCtrl.create({
      content: 'Cargando...'
    });

    this.loader.present();

  }
  public ToastError(mensaje) {
    let toast = this.toastCtrl.create({
      message: mensaje,
      duration: 2000,
      position: 'bottom',
      cssClass: 'toastError'
    });
    toast.onDidDismiss(() => {
      
    });

    toast.present();
  }
  public ToastErrorTime(mensaje, time) {
    let toast = this.toastCtrl.create({
      message: mensaje,
      duration: time,
      position: 'bottom',
      cssClass: 'toastError'
    });
    toast.onDidDismiss(() => {
      
    });

    toast.present();
  }

  public ToastAlerta(mensaje) {
    let toast = this.toastCtrl.create({
      message: mensaje,
      duration: 2000,
      position: 'bottom',
      cssClass: 'toastAlerta'
    });
    toast.onDidDismiss(() => {
      
    });

    toast.present();
  }

  public ToastAlertaTime(mensaje, time) {
    let toast = this.toastCtrl.create({
      message: mensaje,
      duration: time,
      position: 'bottom',
      cssClass: 'toastAlerta'
    });
    toast.onDidDismiss(() => {
      
    });

    toast.present();
  }

  public ToastSuccess(mensaje) {
    let toast = this.toastCtrl.create({
      message: mensaje,
      duration: 2000,
      position: 'bottom',
      cssClass: 'toastSuccess'
    });
    toast.onDidDismiss(() => {
      
    });

    toast.present();
  }

  public ToastSuccessTime(mensaje, time) {
    let toast = this.toastCtrl.create({
      message: mensaje,
      duration: time,
      position: 'bottom',
      cssClass: 'toastSuccess'
    });
    toast.onDidDismiss(() => {
      
    });

    toast.present();
  }
}
