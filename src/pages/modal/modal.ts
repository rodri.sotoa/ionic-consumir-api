import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { ModalProvider } from '../../providers/modal/modal';

/**
 * Generated class for the ModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
})
export class ModalPage {
  public Incidente: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public _ModalProvider:ModalProvider,public viewCtrl:ViewController) {
    this.GETIncidente(navParams.get('Cod'));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalPage');
  }
  CerrarModal(){
    this.viewCtrl.dismiss();
  }
  GETIncidente(id) {
    this._ModalProvider.GETIncidente(id).subscribe(
      result => {
        if (result.status != 200) {
          console.log(result);
          //this._GlobalProvider.loader.dismiss();
        } else {
          this.Incidente = result.body;
          console.log(this.Incidente);
          //this.GETObservacion();
        }
      },
      error => {
        //this._GlobalProvider.loader.dismiss();
        //this._GlobalProvider.ToastError("no se obtuvieron los datos");
        console.log(<any>error);
      }
    );
  }
}
