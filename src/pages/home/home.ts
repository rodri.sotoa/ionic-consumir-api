import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';
import { UsuarioProvider } from '../../providers/usuario/usuario';
import { GlobalProvider } from '../../providers/global/global';
import { IncidenteListadoPage } from '../incidente-listado/incidente-listado';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public usuario: any;
  public password: any;
  //private loged: any;
  constructor(public navCtrl: NavController, public _UsuarioProvider: UsuarioProvider, public _GlobalProvider: GlobalProvider,
    private storage: Storage,private menuCtrl:MenuController) {
    this.validaLogin();
    this.menuCtrl.enable(false);
  }
  credenciales() {
    var json = '';
    json += '{';
    json += ' "user": "' + this.usuario + '" ,';
    json += ' "password": "' + this.password + '"';
    json += '}';
    var j = JSON.stringify(json);
    j = JSON.parse(j);
    console.log(j);
    return j;
  }
  validaLogin() {
    if (!this.storage) {
      console.log('no logeado');
    } else {
      this.storage.get('user').then((val) => {
        if (val != null) {
          return this.navCtrl.setRoot(IncidenteListadoPage);
        } else {
          console.log('no logeado');
        }
      });
    }
  }
  login() {
    //this.storage.set('user', true);
    //return this.navCtrl.setRoot(IncidenteListadoPage);
    console.log(this.usuario+" "+this.password);
    this._UsuarioProvider.POSTLoginSAP(this.usuario,this.password).subscribe(
      result => {
        console.log(result);
        if (result.status != 200 ) {
          this.storage.set('user', false);
          //return this.navCtrl.setRoot(IncidenteListadoPage);
        } else {
          this._GlobalProvider.ToastSuccess("Correcto");
          this.storage.set('user', true);
          return this.navCtrl.setRoot(IncidenteListadoPage);
          //return this.navCtrl.push(IncidenteAddPage);
        }
      },
      error => {
        this._GlobalProvider.ToastError("Error");
        console.log(<any>error);
      }
    );
  }
}
