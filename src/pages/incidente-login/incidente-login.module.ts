import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IncidenteLoginPage } from './incidente-login';

@NgModule({
  declarations: [
    IncidenteLoginPage,
  ],
  imports: [
    IonicPageModule.forChild(IncidenteLoginPage),
  ],
})
export class IncidenteLoginPageModule {}
