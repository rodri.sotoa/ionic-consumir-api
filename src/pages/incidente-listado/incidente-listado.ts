import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { IncidenteProvider } from '../../providers/incidente/incidente';
import { ModalController } from 'ionic-angular';
import { ModalPage } from '../modal/modal';

/**
 * Generated class for the IncidenteListadoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-incidente-listado',
  templateUrl: 'incidente-listado.html',
})
export class IncidenteListadoPage {
  public Incidentes: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public _IncidenteProvider:IncidenteProvider,
    public modalCtrl:ModalController,private menuCtrl:MenuController) {
    this.GETIncidentes();
    this.menuCtrl.enable(true);
  }
  presentModal(id){
    let modal = this.modalCtrl.create(ModalPage,{ Cod: id });
    modal.present();
  }
  GETIncidentes() {
    this._IncidenteProvider.GETIncidente().subscribe(
      result => {
        if (result.status != 200) {
          console.log(result);
          //this._GlobalProvider.loader.dismiss();
        } else {
          console.log(result.body);
          this.Incidentes = result.body;
          //this.GETObservacion();
        }
      },
      error => {
        //this._GlobalProvider.loader.dismiss();
        //this._GlobalProvider.ToastError("no se obtuvieron los datos");
        console.log(<any>error);
      }
    );
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad IncidenteListadoPage');
  }

}
