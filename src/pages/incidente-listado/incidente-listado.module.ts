import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IncidenteListadoPage } from './incidente-listado';

@NgModule({
  declarations: [
    IncidenteListadoPage,
  ],
  imports: [
    IonicPageModule.forChild(IncidenteListadoPage),
  ],
})
export class IncidenteListadoPageModule {}
