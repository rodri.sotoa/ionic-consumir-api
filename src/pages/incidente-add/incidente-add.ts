import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { IncidenteProvider } from '../../providers/incidente/incidente';
import { GlobalProvider } from '../../providers/global/global';
import { LiderProvider } from '../../providers/lider/lider';
import { RespuestaProvider } from '../../providers/respuesta/respuesta';
import { DetalleriesgoProvider } from '../../providers/detalleriesgo/detalleriesgo';
import { IncidenteListadoPage } from '../incidente-listado/incidente-listado'
import { ZonaProvider } from '../../providers/zona/zona';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the IncidenteAddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-incidente-add',
  templateUrl: 'incidente-add.html',
})
export class IncidenteAddPage {
  public Zonas: any;
  public Desc: any;
  public Causa: any;
  public Lider: any;
  public Fecha: Date;
  public Operacion: any;
  public Alcance: any;
  public Ambiente: any;
  public Salud: any;
  public Cargo: any;
  public Gravedad: any;
  public Riesgos: any;
  public Respuestas: any;
  public Lideres: any;
  public riesgo: any;
  public codRiesgo: any;
  public zona: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public _IncidenteProvider: IncidenteProvider,
    public _GlobalProvider: GlobalProvider, public _LiderProvider: LiderProvider, public _RespuestaProvider: RespuestaProvider,
    public _DetalleriesgoProvider: DetalleriesgoProvider, public _ZonaProvider: ZonaProvider, private storage: Storage) {
    this.GETZonas();
    this.GETRiesgos();
    this.GETRespuestas();
    this.GETLider();
  }
  onClick(riesgo, cod) {
    this.riesgo = riesgo;
    this.codRiesgo = cod;
  }
  onCargo($event) {
    this.Cargo = $event;
  }
  onOperacion($event) {
    this.Operacion = $event;
  }
  onAlcance($event) {
    this.Alcance = $event;
  }
  onSalud($event) {
    this.Salud = $event;
  }
  onAmbiente($event) {
    this.Ambiente = $event;
    this.Gravedad = Math.round((parseInt(this.Cargo) + parseInt(this.Operacion) + parseInt(this.Alcance) + parseInt(this.Salud) +
      parseInt(this.Ambiente)) / 5);
    console.log(this.Gravedad);
  }
  onLider($event) {
    this.Lider = $event;
  }
  onChange($event) {
    this.zona = $event;
    //console.log($event.target.value);
  }
  validarCampos(){
    this.Gravedad = Math.round((parseInt(this.Cargo) + parseInt(this.Operacion) + parseInt(this.Alcance) + parseInt(this.Salud) +
      parseInt(this.Ambiente)) / 5);
    if(this.zona == null || this.Gravedad == null || this.Desc== null || this.Causa == null || this.Lider == null ||
      this.codRiesgo == null || this.Fecha == null || this.Cargo == null || this.Operacion == null || this.Alcance == null ||
      this.Salud == null || this.Ambiente == null){
        return false;
    }else{
      return true;
    }
  }
  IncidentePOST() {
    //console.log("ambiente: "+ambiente);
    this.Gravedad = Math.round((parseInt(this.Cargo) + parseInt(this.Operacion) + parseInt(this.Alcance) + parseInt(this.Salud) +
      parseInt(this.Ambiente)) / 5);
    var f = new Date(this.Fecha);
    console.log(f);
    var json = '';
    json += '{';
    json += 'Estado: ' + 1 + ' ,';
    json += ' CodZona: ' + this.zona + ' ,';
    json += ' Gravedad: ' + this.Gravedad + ' ,';
    json += ' Descripcion: "' + this.Desc + '" ,';
    json += ' Causa: "' + this.Causa + '" ,';
    json += ' CodLider: ' + this.Lider + ' ,';
    json += ' CodRiesgo: ' + this.codRiesgo + ' ,';
    json += ' Motivo: "' + this.riesgo + '" ,';
    json += ' FechaIncidente: "' + this.Fecha + '"';
    json += '}';
    var j = JSON.stringify(json);
    j = JSON.parse(j);
    return j;
  }
  POSTIncidente() {
    if(this.validarCampos() == true){
      this._IncidenteProvider.SETIncidente(this.IncidentePOST()).subscribe(
        result => {
          if (result.status != 200) {
            this._GlobalProvider.ToastError("Error");
            //this._GlobalProvider.loader.dismiss();
          } else {
            this._GlobalProvider.ToastSuccess("Incidente agregado correctamente");
            this.navCtrl.setRoot(IncidenteListadoPage);
          }
        },
      );
    }else{
      this._GlobalProvider.ToastError("Error Complete los campos");
    }
    
  }
  GETLider() {
    this._LiderProvider.GETLider().subscribe(
      result => {
        if (result.status != 200) {
          console.log(result);
          
        } else {

          this.Lideres = result.body;
          //this.GETObservacion();
        }
      },
      error => {
        //this._GlobalProvider.loader.dismiss();
        //this._GlobalProvider.ToastError("no se obtuvieron los datos");
        console.log(<any>error);
      }
    );
  }
  GETRespuestas() {
    this._RespuestaProvider.GETRespuesta().subscribe(
      result => {
        if (result.status != 200) {
          console.log(result);
          //this._GlobalProvider.loader.dismiss();
        } else {

          this.Respuestas = result.body;
          //this.GETObservacion();
        }
      },
      error => {
        //this._GlobalProvider.loader.dismiss();
        //this._GlobalProvider.ToastError("no se obtuvieron los datos");
        console.log(<any>error);
      }
    );
  }
  GETRiesgos() {
    this._DetalleriesgoProvider.GETRiesgo().subscribe(
      result => {
        if (result.status != 200) {
          console.log(result);
          //this._GlobalProvider.loader.dismiss();
        } else {
          //console.log(result.body);
          this.Riesgos = result.body;
          //this.GETObservacion();
        }
      },
      error => {
        //this._GlobalProvider.loader.dismiss();
        //this._GlobalProvider.ToastError("no se obtuvieron los datos");
        console.log(<any>error);
      }
    );
  }
  GETZonas() {
    this._ZonaProvider.GETZona().subscribe(
      result => {
        if (result.status != 200) {
          console.log(result);
          //this._GlobalProvider.loader.dismiss();
        } else {
          //console.log(result.body);
          this.Zonas = result.body;

          //this.GETObservacion();
        }
      },
      error => {
        //this._GlobalProvider.loader.dismiss();
        //this._GlobalProvider.ToastError("no se obtuvieron los datos");
        console.log(<any>error);
      }
    );
  }
  validaLogin(){
    this.storage.get('user').then((val) => {
      if(val != null){
        console.log('logeado');
      }else{
        console.log('no logeado');
      }
    });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad IncidenteAddPage');
    this.validaLogin();
  }

}
