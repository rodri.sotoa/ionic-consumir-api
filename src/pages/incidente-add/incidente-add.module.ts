import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IncidenteAddPage } from './incidente-add';

@NgModule({
  declarations: [
    IncidenteAddPage,
  ],
  imports: [
    IonicPageModule.forChild(IncidenteAddPage),
  ],
})
export class IncidenteAddPageModule {}
