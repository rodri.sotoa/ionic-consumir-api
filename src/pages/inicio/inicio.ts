import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the InicioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-inicio',
  templateUrl: 'inicio.html',
})
export class InicioPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,private storage:Storage,private menuCtrl:MenuController) {
    this.menuCtrl.enable(true);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InicioPage');
    this.validaLogin();
  }
  validaLogin(){
    this.storage.get('user').then((val) => {
      if(val != null){
        console.log('logeado');
      }else{
        this.storage.set('user',null);
        return this.navCtrl.setRoot(HomePage);
      }
    });
  }
}
