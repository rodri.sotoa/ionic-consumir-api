import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { IncidenteListadoPage } from '../pages/incidente-listado/incidente-listado';
import { IncidenteAddPage } from '../pages/incidente-add/incidente-add';
import { InicioPage } from '../pages/inicio/inicio';
import { ModalPage } from '../pages/modal/modal';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { DetalleriesgoProvider } from '../providers/detalleriesgo/detalleriesgo';
import { GlobalProvider } from '../providers/global/global';
import { IncidenteProvider } from '../providers/incidente/incidente';
import { LiderProvider } from '../providers/lider/lider';
import { PuntosgeograficosProvider } from '../providers/puntosgeograficos/puntosgeograficos';
import { RespuestaProvider } from '../providers/respuesta/respuesta';
import { ZonaProvider } from '../providers/zona/zona';
import { UsuarioProvider } from '../providers/usuario/usuario';
import { IonicStorageModule } from '@ionic/storage';
import { ModalProvider } from '../providers/modal/modal';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    IncidenteListadoPage,
    IncidenteAddPage,
    InicioPage,
    ModalPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    IncidenteListadoPage,
    IncidenteAddPage,
    InicioPage,
    ModalPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DetalleriesgoProvider,
    GlobalProvider,
    IncidenteProvider,
    LiderProvider,
    PuntosgeograficosProvider,
    RespuestaProvider,
    ZonaProvider,
    UsuarioProvider,
    ModalProvider
  ]
})
export class AppModule {}
